import {Component, OnInit, TemplateRef} from '@angular/core';
import {TokenStorageService} from "../auth/token-storage.service";
import {Router} from "@angular/router";
import {BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {

  modalRef: BsModalRef;
  message: string;

  constructor(private token: TokenStorageService,
              private router: Router,
              private modalService: BsModalService) { }

  ngOnInit() { }


  logout() {
    this.token.signOut();
    this.router.navigate([('login')]);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  confirm(): void {
    this.message = 'Confirmed!';
    this.modalRef.hide();
  }

  decline(): void {
    this.modalRef.hide();
  }

}
