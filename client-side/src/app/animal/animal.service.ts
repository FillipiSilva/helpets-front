import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:8080/api/v1/animals';

  getAnimal(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createAnimal(animal: Object): Observable<Object> {
    return this.http.post<Object>(`${this.baseUrl}`, animal);
  }

  updateAnimal(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteAnimal(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getAnimalList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
