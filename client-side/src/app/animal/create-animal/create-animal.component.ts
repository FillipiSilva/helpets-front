import {Component, OnInit, TemplateRef} from '@angular/core';
import {Animal} from '../animal';
import {AnimalService} from '../animal.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../auth/token-storage.service';
import {BsModalService, BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'app-create-animal',
  templateUrl: './create-animal.component.html',
  styleUrls: ['./create-animal.component.css']
})
export class CreateAnimalComponent implements OnInit {

  modalRef: BsModalRef
  animal: Animal = new Animal();
  submitted = false;

  constructor(private animalService: AnimalService,
              private router: Router,
              private token: TokenStorageService) { }

  ngOnInit() {
  }

  newAnimal(): void {
    this.submitted = false;
    this.animal = new Animal();
  }

  save() {
    this.animalService.createAnimal(this.animal)
      .subscribe(data => console.log(data), error => console.log(error));
    this.animal = new Animal();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/animals']);
  }


}
