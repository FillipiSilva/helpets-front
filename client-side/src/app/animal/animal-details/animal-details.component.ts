import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Animal} from '../animal';
import {AnimalService} from '../animal.service';

@Component({
  selector: 'app-animal-details',
  templateUrl: './animal-details.component.html',
  styleUrls: ['./animal-details.component.css']
})
export class AnimalDetailsComponent implements OnInit {

  id: number;
  animal: Animal;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private animalService: AnimalService) { }

  ngOnInit() {
    this.animal = new Animal();

    this.id = this.route.snapshot.params['id'];

    this.animalService.getAnimal(this.id)
      .subscribe(data => {
        console.log(data)
        this.animal = data;
      }, error => console.log(error));
  }

  listAnimal(){
    this.router.navigate(['animals']);
  }
}
