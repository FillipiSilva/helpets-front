import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {Animal} from '../animal';
import {AnimalService} from '../animal.service';

@Component({
  selector: "app-animal-list",
  templateUrl: "./animal-list.component.html",
  styleUrls: ["./animal-list.component.css"]
})
export class AnimalListComponent implements OnInit {
  animals: Observable<Animal[]>;

  constructor(private animalService: AnimalService,
              private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.animals = this.animalService.getAnimalList();
  }

  deleteAnimal(id: number) {
    this.animalService.deleteAnimal(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  animalDetails(id: number){
    this.router.navigate(['detailsAnimal', id]);
  }
}
