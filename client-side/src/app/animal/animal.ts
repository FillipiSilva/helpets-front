export class Animal {

  id: number;
  type: string;
  animalName: string;
  age: number;
  breed: string;
  size: number;
  mainColor: string;
  weight: number;
  vaccine: boolean;
}
