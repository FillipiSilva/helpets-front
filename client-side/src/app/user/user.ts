export class User {

  id: number;
  username: string;
  password: string;
  email: string;
  cpf: string;
  firstName: string;
  lastName: string;
  address: string;
  number: string;
  neighborhood: string;
  city: string;
  state: string;
  zipCode: string;
  birthDate: any;
  phone: string;
  imageUrl: string;
  frequency: any;

}
