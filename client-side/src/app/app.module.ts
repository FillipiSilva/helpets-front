import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserDetailsComponent } from './user/user-details/user-details.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { CreateUserComponent } from './user/create-user/create-user.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { CreateAnimalComponent } from './animal/create-animal/create-animal.component';
import { AnimalDetailsComponent } from './animal/animal-details/animal-details.component';
import { AnimalListComponent } from './animal/animal-list/animal-list.component';
import { CreateDonationComponent } from './donation/create-donation/create-donation.component';
import { DonationDetailsComponent } from './donation/donation-details/donation-details.component';
import { DonationListComponent } from './donation/donation-list/donation-list.component';
import { CreateCommentComponent } from './comment/create-comment/create-comment.component';
import { CommentDetailsComponent } from './comment/comment-details/comment-details.component';
import { CommentListComponent } from './comment/comment-list/comment-list.component';
import { CreatePostComponent } from './post/create-post/create-post.component';
import { PostDetailsComponent } from './post/post-details/post-details.component';
import { PostListComponent } from './post/post-list/post-list.component';
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {FooterComponent} from "./shared/footer/footer.component";
import {HeaderComponent} from "./shared/header/header.component";
import {RegisterComponent} from "./register/register.component";
import {OrderModule} from 'ngx-order-pipe';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {httpInterceptorProviders} from './auth/auth-interceptor';
import { FeedComponent } from './feed/feed.component';
import { SobreComponent } from './sobre/sobre.component';
import { PetsComponent } from './pets/pets.component';
import { ContatoComponent } from './contato/contato.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AdminComponent } from './admin/admin.component';
import { AlertModule } from 'ngx-bootstrap/alert';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserDetailsComponent,
    UserListComponent,
    CreateUserComponent,
    CreateAnimalComponent,
    AnimalDetailsComponent,
    AnimalListComponent,
    CreateDonationComponent,
    DonationDetailsComponent,
    DonationListComponent,
    CreateCommentComponent,
    CommentDetailsComponent,
    CommentListComponent,
    CreatePostComponent,
    PostDetailsComponent,
    PostListComponent,
    LoginComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    RegisterComponent,
    FeedComponent,
    SobreComponent,
    PetsComponent,
    ContatoComponent,
    AdminComponent,



    /*

    PessoaEditComponent
    RegisterComponent,
     */
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    OrderModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot()
  ],
  providers: [
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
