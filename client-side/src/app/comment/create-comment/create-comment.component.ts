import {Component, OnInit} from '@angular/core';
import {CommentService} from '../comment.service';
import {Router} from '@angular/router';
import {Comment} from '../comment';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.css']
})
export class CreateCommentComponent implements OnInit {

  comment: Comment = new Comment();
  submitted = false;

  constructor(private commentService: CommentService,
              private router: Router) { }

  ngOnInit() {
  }

  newComment(): void {
    this.submitted = false;
    this.comment = new Comment();
  }

  save() {
    this.commentService.createComment(this.comment)
      .subscribe(data => console.log(data), error => console.log(error));
    this.comment = new Comment();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/comments']);
  }

}
