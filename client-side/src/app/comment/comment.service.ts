import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:8080/api/v1/comments';

  getComment(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createComment(comment: Object): Observable<Object> {
    return this.http.post<Object>(`${this.baseUrl}`, comment);
  }

  updateComment(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteComment(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getCommentsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
