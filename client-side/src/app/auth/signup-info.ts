export class SignUpInfo {

  id: number;
  name: string;
  username: string;
  email: string;
  password: string;
  cpf: string;
  addRess: string;
  number: string;
  neighborhood: string;
  city: string;
  state: string;
  zipCode: string;
  birthDate: any;
  phones: string;
  imageUrl: string;
  frequency: any;


  constructor(name: string, username: string, email: string, password: string, cpf: string, addRess: string, number: string, neighborhood: string, city: string, state: string, zipCode: string, birthDate: any, phones: string) {
    this.name = name;
    this.username = username;
    this.email = email;
    this.password = password;
    this.cpf = cpf;
    this.addRess = addRess;
    this.number = number;
    this.neighborhood = neighborhood;
    this.city = city;
    this.state = state;
    this.zipCode = zipCode;
    this.birthDate = birthDate;
    this.phones = phones;
  }
}
