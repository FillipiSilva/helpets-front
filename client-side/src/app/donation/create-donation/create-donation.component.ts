import {Component, OnInit} from '@angular/core';
import {Donation} from '../donation';
import {DonationService} from '../donation.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-donation',
  templateUrl: './create-donation.component.html',
  styleUrls: ['./create-donation.component.css']
})
export class CreateDonationComponent implements OnInit {

  donation: Donation = new Donation();
  submitted = false;

  constructor(private donationService: DonationService,
              private router: Router) { }

  ngOnInit() {
  }

  newDonation(): void {
    this.submitted = false;
    this.donation = new Donation();
  }

  save() {
    this.donationService.createDonation(this.donation)
      .subscribe(data => console.log(data), error => console.log(error));
    this.donation = new Donation();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/donations']);
  }
}
