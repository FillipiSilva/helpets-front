import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DonationService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:8080/api/v1/donations';

  getDonation(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createDonation(donation: Object): Observable<Object> {
    return this.http.post<Object>(`${this.baseUrl}`, donation);
  }

  updateDonation(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteDonation(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getDonationsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
