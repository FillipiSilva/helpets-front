import {Component, OnInit} from '@angular/core';
import {Donation} from '../donation';
import {ActivatedRoute, Router} from '@angular/router';
import {DonationService} from '../donation.service';

@Component({
  selector: 'app-donation-details',
  templateUrl: './donation-details.component.html',
  styleUrls: ['./donation-details.component.css']
})
export class DonationDetailsComponent implements OnInit {

  id: number;
  donation: Donation;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private donationService: DonationService) { }

  ngOnInit() {
    this.donation = new Donation();

    this.id = this.route.snapshot.params['id'];

    this.donationService.getDonation(this.id)
      .subscribe(data => {
        console.log(data)
        this.donation = data;
      }, error => console.log(error));
  }

  listDonations(){
    this.router.navigate(['donations']);
  }

}
