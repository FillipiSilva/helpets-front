import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Donation} from '../donation';
import {DonationService} from '../donation.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-donation-list',
  templateUrl: './donation-list.component.html',
  styleUrls: ['./donation-list.component.css']
})
export class DonationListComponent implements OnInit {

  donations: Observable<Donation[]>;

  constructor(private donationService: DonationService,
              private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.donations = this.donationService.getDonationsList();
  }

  deleteDonation(id: number) {
    this.donationService.deleteDonation(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  donationDetails(id: number){
    this.router.navigate(['detailsDonation', id]);
  }

}
