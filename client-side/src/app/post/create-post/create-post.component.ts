import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Post} from '../post';
import {PostService} from '../post.service';
import {Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  posts: Observable<Post[]>;
  post: Post = new Post();
  submitted = false;
  key: 'nome';
  reverse: true;
  modalRef: BsModalRef;
  message: string;

  constructor(private postService: PostService,
              private router: Router,
              private modalService: BsModalService) { }


  newPost(): void {
    this.submitted = false;
    this.post = new Post();
  }

  save() {
    this.postService.createPost(this.post)
      .subscribe(data => console.log(data), error => console.log(error));
    this.post = new Post();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }


  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.posts = this.postService.getPostsList();
  }


}
