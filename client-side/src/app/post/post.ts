export class Post {

  id: number;
  title: string;
  postImage: string;
  content: string;
  postData: any;

}
