import {Component, OnInit, TemplateRef} from '@angular/core';
import {Observable} from 'rxjs';
import {Post} from '../post';
import {PostService} from '../post.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../auth/token-storage.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  posts: Observable<Post[]>;
  key: 'post';
  reverse: true;
  modalRef: BsModalRef;
  message: string;

  constructor(private postService: PostService,
              private token: TokenStorageService,
              private router: Router,
              private modalService: BsModalService) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.posts = this.postService.getPostsList();
  }

  deletePost(id: number) {
    this.postService.deletePost(id)
      .subscribe(
        data => {
          console.log(data);
          this.modalRef.hide();
          this.router.navigate(['feed']);
        },
        error => console.log(error));
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }


  decline(): void {
    this.modalRef.hide();
    this.router.navigate(['feed']);
  }

}
