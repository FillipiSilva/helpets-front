import {RouterModule, Routes} from "@angular/router";
import {UserListComponent} from "./user/user-list/user-list.component";
import {CreateUserComponent} from "./user/create-user/create-user.component";
import {UserDetailsComponent} from "./user/user-details/user-details.component";
import {NgModule} from "@angular/core";
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {AnimalListComponent} from './animal/animal-list/animal-list.component';
import {CreateAnimalComponent} from './animal/create-animal/create-animal.component';
import {AnimalDetailsComponent} from './animal/animal-details/animal-details.component';
import {DonationListComponent} from './donation/donation-list/donation-list.component';
import {CreateDonationComponent} from './donation/create-donation/create-donation.component';
import {DonationDetailsComponent} from './donation/donation-details/donation-details.component';
import {CommentListComponent} from './comment/comment-list/comment-list.component';
import {CreateCommentComponent} from './comment/create-comment/create-comment.component';
import {CommentDetailsComponent} from './comment/comment-details/comment-details.component';
import {PostListComponent} from './post/post-list/post-list.component';
import {CreatePostComponent} from './post/create-post/create-post.component';
import {PostDetailsComponent} from './post/post-details/post-details.component';
import {SobreComponent} from './sobre/sobre.component';
import {PetsComponent} from './pets/pets.component';
import {ContatoComponent} from './contato/contato.component';
import {FeedComponent} from './feed/feed.component';
import {AdminComponent} from './admin/admin.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'users', component: UserListComponent },
  { path: 'cadastro', component: CreateUserComponent },
  { path: 'detailsUser/:id', component: UserDetailsComponent },
  // { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: LoginComponent},
  // { path: '', redirectTo: 'animal', pathMatch: 'full' },
  { path: 'animals', component: AnimalListComponent },
  { path: 'cadastro-pets', component: CreateAnimalComponent },
  { path: 'detailsAnimal/:id', component: AnimalDetailsComponent },
  // { path: '', redirectTo: 'donation', pathMatch: 'full' },
  { path: 'donations', component: DonationListComponent },
  { path: 'addDonation', component: CreateDonationComponent },
  { path: 'detailsDonation/:id', component: DonationDetailsComponent },
  // { path: '', redirectTo: 'comment', pathMatch: 'full' },
  { path: 'comments', component: CommentListComponent },
  { path: 'addComment', component: CreateCommentComponent },
  { path: 'detailsComment/:id', component: CommentDetailsComponent },
  // { path: '', redirectTo: 'post', pathMatch: 'full' },
  { path: 'posts', component: PostListComponent },
  { path: 'addPost', component: CreatePostComponent },
  { path: 'detailsPost/:id', component: PostDetailsComponent },


  { path: 'feed', component: FeedComponent},
  { path: 'sobre', component: SobreComponent },


  { path: 'sobre', component: SobreComponent },
  { path: 'pets', component: PetsComponent },
  { path: 'contato', component: ContatoComponent },
  { path: 'feed', component: FeedComponent },
  { path: 'admin', component: AdminComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

