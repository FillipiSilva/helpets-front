import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../auth/token-storage.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private token: TokenStorageService,
              private router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.token.signOut();
    this.router.navigate([('login')]);
  }

}
